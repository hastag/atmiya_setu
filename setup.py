from setuptools import setup, find_packages

with open("requirements.txt") as f:
    install_requires = f.read().strip().split("\n")

# get version from __version__ variable in atmiya_setu/__init__.py
from atmiya_setu import __version__ as version

setup(
    name="atmiya_setu",
    version=version,
    description="Atmiya Setu Management App",
    author="Castlecraft Ecommerce Pvt. Ltd.",
    author_email="support@castlecraft.in",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)
