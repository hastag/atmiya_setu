import frappe, json
import random
import urllib.request
import urllib.parse
from frappe.utils import cint

@frappe.whitelist(allow_guest=True)
def get(mobile_no=None):
	def generate_otp():
		otp = ''.join(["{}".format(random.randint(0, 9)) for i in range(0, otp_length)])
		return {"id": key, "otp": otp, "timestamp": str(frappe.utils.get_datetime().utcnow())}

	if not mobile_no:
		mobile_no = frappe.form_dict.get("mobile_no")
		if not mobile_no:
			frappe.throw("NOMOBILE", exc=LookupError)

	u = frappe.db.get_value("User", {"mobile_no": mobile_no}, "name")

	if not u:
		frappe.throw("USERNOTFOUND", exc=LookupError)

	key = mobile_no + "_otp"	
	otp_length = 6 # 6 digit OTP
	rs = frappe.cache()

	settings = frappe.get_single("Login settings")

	if rs.get_value(key) and otp_not_expired(rs.get_value(key)): # check if an otp is already being generated
		otp_json = rs.get_value(key)
	else:
		otp_json = generate_otp()
		rs.set_value(key, otp_json)

	"""
	FIRE SMS FOR OTP
		"{0} is your OTP for AgriNext. Do not share OTP with anybody. Thanks.".format(otp_json.get("otp"))
	"""
	# key = settings.get_password("textlocal_key")
	sender = "Stmiyasetu"
	message = "OTPGENERATED:{0}".format(otp_json.get("otp"))
	# sendSMS(key,mobile_no,sender,message)
	# return "OTPGENERATED:{0}".format(otp_json.get("otp")) # MUST DISABLE IN PRODUCTION!!
	email = frappe.db.get_value("User", {"mobile_no": mobile_no}, "email")
	frappe.sendmail(recipients=email,
				subject=("Atmiyasetu login OTP"),
				message="OTPGENERATED:{0}".format(otp_json.get("otp"))
			)

@frappe.whitelist(allow_guest=True)
def authenticate(otp=None, mobile_no=None, client_id=None):
	if not otp:
		otp = frappe.form_dict.get("otp")
		if not otp:
			frappe.throw("NOOTP")

	if not mobile_no:
		mobile_no = frappe.form_dict.get("mobile_no")
		if not mobile_no:
			frappe.throw("NOMOBILENO")

	# if not client_id:
	# 	client_id = frappe.form_dict.get("client_id")
	# 	if not client_id:
	# 		frappe.throw("NOCLIENTID")

	rs = frappe.cache()
	otp_json = rs.get_value("{0}_otp".format(mobile_no))

	if otp_json.get("otp") != otp:
		frappe.throw("OTPNOTFOUND")

	if not otp_not_expired(otp_json):
		frappe.throw("OTPEXPIRED")

	otoken = create_bearer_token(mobile_no, client_id)
	
	out = {
		"access_token": otoken.access_token,
		"refresh_token": otoken.refresh_token,
		"expires_in": otoken.expires_in,
		"scope": otoken.scopes
	}

	# Delete consumed otp
	rs.delete_key(mobile_no + "_otp")

	frappe.local.response = frappe._dict(out)

def create_bearer_token(mobile_no, client_id):
	otoken = frappe.new_doc("OAuth Bearer Token")
	otoken.access_token = frappe.generate_hash(length=30)
	otoken.refresh_token = frappe.generate_hash(length=30)
	otoken.user = frappe.db.get_value("User", {"mobile_no": mobile_no}, "name")
	otoken.scopes = "all"
	otoken.client = client_id
	otoken.redirect_uri = frappe.db.get_value("OAuth Client", client_id, "default_redirect_uri")
	otoken.expires_in = 3600
	otoken.save(ignore_permissions=True)
	frappe.db.commit()

	return otoken

def otp_not_expired(otp_json):
	flag = True
	diff = frappe.utils.get_datetime().utcnow() - frappe.utils.get_datetime(otp_json.get("timestamp"))
	if int(diff.seconds) / 60 >= 10:
		flag = False

	return flag

@frappe.whitelist(allow_guest=True)
def aa():
	def sendSMS(apikey, numbers, sender, message):
		data =  urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers,
			'message' : message, 'sender': sender})
		data = data.encode('utf-8')
		request = urllib.request.Request("https://api.textlocal.in/send/?")
		f = urllib.request.urlopen(request, data)
		fr = f.read()
		return(f)
	resp =  sendSMS('4E0zw45vOF4-W4dHxImTFOyKPkzjooniB4mxrLmLGR', '918999400385',
    'JVSENA', 'Hi there, thank you for sending your first test message from Textlocal. Get 20% off today with our code:')
	return resp

@frappe.whitelist(allow_guest=True)
def sign_up(
    email,
    first_name,
    last_name,
    mobile_no,
    password,
    role
):
    user = frappe.new_doc("User")
    user.first_name = first_name
    user.last_name = last_name
    user.email = email
    user.mobile_no = mobile_no
    user.send_welcome_email = 0
    user.save(ignore_permissions=True)
    user.new_password = password
    roles = user.append("roles")
    roles.role = role
    user.save(ignore_permissions=True)
    frappe.db.commit()
    # login(mobile_no,password)
    return user
