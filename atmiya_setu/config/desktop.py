from frappe import _


def get_data():
    return [
        {
            "module_name": "Atmiya Setu",
            "color": "grey",
            "icon": "octicon octicon-file-directory",
            "type": "module",
            "label": _("Atmiya Setu"),
        }
    ]
