# Copyright (c) 2022, Castlecraft Ecommerce Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe.website.website_generator import WebsiteGenerator


class Devotee(WebsiteGenerator):
    def autoname(self):
        self.name = frappe.generate_hash(length=6).upper()
    
    def before_insert(self):
        user = frappe.get_doc("User",frappe.session.user)
        for role in user.roles:
            if role.role == "Satsangi":
                form_avail = frappe.db.exists("Devotee", {"owner": frappe.session.user})
                if form_avail:
                    frappe.throw("Form is created for this user")
