// Copyright (c) 2022, Castlecraft Ecommerce Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Devotee', {
	refresh: function(frm) {
		frm.set_df_property('sb_govt_service_details', 'hidden', 1);
		frm.set_df_property('sb_private_business_details', 'hidden', 1);
		frm.set_df_property('service_type', 'hidden', 1);
	},
	occupation: function(frm) {
		if (frm.doc.occupation === 'Service') {
			frm.set_df_property('service_type', 'hidden', 0);
		} else if (frm.doc.occupation === 'Business') {
			frm.set_df_property('sb_govt_service_details', 'hidden', 1);
			frm.set_df_property('sb_private_business_details', 'hidden', 0);
		} else {
			frm.set_df_property('service_type', 'hidden', 1);
			frm.set_df_property('sb_govt_service_details', 'hidden', 1);
			frm.set_df_property('sb_private_business_details', 'hidden', 1);
		}
	},
	service_type: function(frm) {
		if (frm.doc.service_type === 'Government') {
			frm.set_df_property('sb_govt_service_details', 'hidden', 0);
			frm.set_df_property('sb_private_business_details', 'hidden', 1);
		} else if (frm.doc.service_type === 'Private') {
			frm.set_df_property('sb_govt_service_details', 'hidden', 1);
			frm.set_df_property('sb_private_business_details', 'hidden', 0);
		}
	}
});
