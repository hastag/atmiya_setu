from __future__ import unicode_literals

import frappe
from urllib import request
import webbrowser

def after_login(login_manager):
    user = frappe.get_doc("User",login_manager.user)
    for i in user.roles:
        if i.role == "Satsangi":
            url = frappe.utils.get_url() + "/devotee"
            webbrowser.open(url)
            # webUrl = request.urlopen(url)