# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

from __future__ import unicode_literals
import frappe
import frappe.utils
from frappe import _

no_cache = True


@frappe.whitelist(allow_guest=True)
def sign_up_with_address(
    email,
    first_name,
    last_name,
    mobile_no,
    password,
    role
):
    user = frappe.new_doc("User")
    user.first_name = first_name
    user.last_name = last_name
    user.email = email
    user.mobile_no = mobile_no
    user.send_welcome_email = 0
    user.save(ignore_permissions=True)
    user.new_password = password
    roles = user.append("roles")
    roles.role = role
    user.save(ignore_permissions=True)
    frappe.db.commit()
    # login(mobile_no,password)
    return frappe.utils.get_url()+"/login?redirect-to=/devotee"

def login(usr, pwd):
    try:
        login_manager = frappe.auth.LoginManager()
        login_manager.authenticate(user=usr, pwd=pwd)
        login_manager.post_login()
    except frappe.exceptions.AuthenticationError:
        frappe.clear_messages()
        frappe.local.response["message"] = {
            "success_key":0,
            "message":"Authentication Error!"
        }

        return