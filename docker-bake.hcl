variable "FRAPPE_VERSION" {
  default = "version-14"
}

variable "REGISTRY_NAME" {
  default = "custom_images"
}

variable "IMAGE_NAME" {
  default = "bench"
}

variable "VERSION" {
  default = "latest"
}

target "default" {
    dockerfile = "images/Dockerfile"
    tags = ["${REGISTRY_NAME}/${IMAGE_NAME}:${VERSION}"]
    args = {
      "FRAPPE_VERSION" = FRAPPE_VERSION
    }
}
